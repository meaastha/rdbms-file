\contentsline {chapter}{\textit {List of Figures}}{iii}{dummy.2}
\contentsline {chapter}{\textit {List of Tables}}{v}{dummy.4}
\contentsline {chapter}{\textit {Table of Contents}}{vii}{dummy.6}
\contentsline {chapter}{\numberline {1}Installation and configuration of PostgreSQL and Pgadmin on machine and preparing to accept connections from all Ip addresses.}{1}{chapter.8}
\contentsline {section}{\numberline {1.1} Installation and conﬁguration of PostgreSQL 9.5 }{1}{section.9}
\contentsline {subsection}{\numberline {1.1.1}Introduction}{1}{subsection.10}
\contentsline {subsection}{\numberline {1.1.2} Add the PostgreSQL repository}{1}{subsection.11}
\contentsline {subsection}{\numberline {1.1.3}Update the package list}{2}{subsection.17}
\contentsline {subsection}{\numberline {1.1.4}PostgreSQL installation}{2}{subsection.20}
\contentsline {subsection}{\numberline {1.1.5}Connecting to the Postgres server}{2}{subsection.24}
\contentsline {section}{\numberline {1.2}Installation and configuration of PgAdminIII}{3}{section.32}
\contentsline {subsection}{\numberline {1.2.1}Connecting to database}{3}{subsection.36}
\contentsline {section}{\numberline {1.3}Configuring PostgreSQL server 9.5 to accept from all IP addresses}{5}{section.47}
\contentsline {chapter}{\numberline {2}Write the queries for Data Definition (CREATE, DROP, ALTER and RENAME) and Data Manipulation Language (SELECT, INSERT, UPDATE and DELETE).}{7}{chapter.71}
\contentsline {section}{\numberline {2.1}Data Definition (CREATE, DROP, ALTER and RENAME)}{7}{section.72}
\contentsline {subsection}{\numberline {2.1.1}CREATE SCHEMA}{7}{subsection.73}
\contentsline {subsection}{\numberline {2.1.2}CREATE TABLE}{7}{subsection.76}
\contentsline {subsection}{\numberline {2.1.3}DROP TABLE}{7}{subsection.85}
\contentsline {subsection}{\numberline {2.1.4}ALTER TABLE}{8}{subsection.89}
\contentsline {subsection}{\numberline {2.1.5}RENAME TABLE}{9}{subsection.94}
\contentsline {section}{\numberline {2.2}Data Manipulation (SELECT, INSERT, UPDATE and DELETE)}{10}{section.98}
\contentsline {subsection}{\numberline {2.2.1}SELECT}{10}{subsection.99}
\contentsline {subsection}{\numberline {2.2.2}INSERT}{11}{subsection.103}
\contentsline {subsection}{\numberline {2.2.3}UPDATE}{12}{subsection.108}
\contentsline {subsection}{\numberline {2.2.4}DELETE}{13}{subsection.114}
\contentsline {chapter}{\numberline {3}Write SQL queries using Logical operators (<,>,=etc).}{15}{chapter.118}
\contentsline {section}{\numberline {3.1}Arithmetic Operators}{15}{section.119}
\contentsline {subsection}{\numberline {3.1.1}Addition operator}{16}{subsection.120}
\contentsline {subsection}{\numberline {3.1.2}Subraction operator}{16}{subsection.124}
\contentsline {subsection}{\numberline {3.1.3}Multiplication operator}{17}{subsection.128}
\contentsline {subsection}{\numberline {3.1.4}Division operator}{18}{subsection.132}
\contentsline {section}{\numberline {3.2}Comparison Operators}{18}{section.136}
\contentsline {section}{\numberline {3.3}Logical Operators}{19}{section.140}
\contentsline {chapter}{\numberline {4}Write SQL queries using SQL operators (Between, AND, OR, IN, Like, NULL).}{21}{chapter.144}
\contentsline {section}{\numberline {4.1}Between Operator}{21}{section.145}
\contentsline {section}{\numberline {4.2}IN Operator}{21}{section.149}
\contentsline {section}{\numberline {4.3}NOT IN Operator}{22}{section.153}
\contentsline {section}{\numberline {4.4}OR Operator}{22}{section.157}
\contentsline {section}{\numberline {4.5}LIKE Operator}{22}{section.161}
\contentsline {section}{\numberline {4.6}NOT LIKE Operator}{23}{section.165}
\contentsline {chapter}{\numberline {5}Write SQL query using character, number, date and group functions.}{25}{chapter.169}
\contentsline {section}{\numberline {5.1}Character}{25}{section.170}
\contentsline {section}{\numberline {5.2}Numeric Functions}{25}{section.171}
\contentsline {subsection}{\numberline {5.2.1}ABS()}{25}{subsection.172}
\contentsline {subsection}{\numberline {5.2.2}ceil()}{26}{subsection.176}
\contentsline {subsection}{\numberline {5.2.3}floor()}{27}{subsection.181}
\contentsline {subsection}{\numberline {5.2.4}round()}{27}{subsection.185}
\contentsline {section}{\numberline {5.3}Date And Time Functions}{28}{section.189}
\contentsline {subsection}{\numberline {5.3.1}Age() Function}{28}{subsection.190}
\contentsline {subsection}{\numberline {5.3.2}Current Date Function()}{28}{subsection.193}
\contentsline {subsection}{\numberline {5.3.3}local time stamp()}{28}{subsection.196}
\contentsline {chapter}{\numberline {6}Write SQL queries for Relational Algebra (UNION, INTERSECT, and MINUS, etc.)}{31}{chapter.202}
\contentsline {section}{\numberline {6.1}UNION}{32}{section.205}
\contentsline {section}{\numberline {6.2}UNION ALL}{32}{section.211}
\contentsline {subsection}{\numberline {6.2.1}Order By}{33}{subsection.217}
\contentsline {section}{\numberline {6.3}INTERSECTION}{34}{section.223}
\contentsline {chapter}{\numberline {7}Write SQL queries for extracting data from more than one table (Inner Join, Outer Join, Cross Join)}{35}{chapter.228}
\contentsline {section}{\numberline {7.1}Inner Join}{35}{section.231}
\contentsline {section}{\numberline {7.2}Left Outer Join}{36}{section.242}
\contentsline {section}{\numberline {7.3}Right Outer Join}{37}{section.253}
\contentsline {section}{\numberline {7.4}Full Outer Join}{38}{section.264}
\contentsline {section}{\numberline {7.5}Cross Join}{39}{section.275}
